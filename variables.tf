# Group
variable "name" {
  type        = string
  description = "The name of the group."
  default     = null
}
variable "path" {
  type        = string
  description = "The path of the group."
}
variable "description" {
  type        = string
  description = "The group’s description."
  default     = null
}
variable "parent_id" {
  type        = number
  description = "The parent group ID for creating nested group."
  default     = null
}

variable "visibility_level" {
  type        = string
  description = "The group’s visibility. Can be `private`, `internal`, or `public`."
  default     = "public"

  validation {
    condition     = contains(["private", "internal", "public"], var.visibility_level)
    error_message = "The visibility_level value must be \"private\", \"internal\", or \"public\"."
  }
}
variable "project_creation_level" {
  type        = string
  description = "Determine if developers can create projects in the group. Can be `noone` (No one), `maintainer` (users with the Maintainer role), or `developer` (users with the Developer or Maintainer role)."
  default     = "maintainer"

  validation {
    condition     = contains(["noone", "maintainer", "developer"], var.project_creation_level)
    error_message = "The project_creation_level value must be \"noone\", \"maintainer\", or \"developer\"."
  }
}
variable "request_access_enabled" {
  type        = string
  description = "Allow users to request member access."
  default     = false
}
variable "subgroup_creation_level" {
  type        = string
  description = "Allowed to create subgroups. Can be `owner` (Owners), or `maintainer` (users with the Maintainer role)."
  default     = "maintainer"

  validation {
    condition     = contains(["owner", "maintainer"], var.subgroup_creation_level)
    error_message = "The subgroup_creation_level value must be \"owner\", or \"maintainer\"."
  }
}

variable "lfs_enabled" {
  type        = bool
  description = "Enable/disable Large File Storage (LFS) for the projects in this group."
  default     = true
}

variable "require_two_factor_authentication" {
  type        = bool
  description = "Require all users in this group to setup Two-factor authentication."
  default     = false
}

# Members
variable "members" {
  type = map(object({
    access_level = string
  }))
  description = <<-EOF
    Group memberships.

    Example:
    {
      <user_id> = {
        access_level = "developer"
      }
    }
    Valid values: "no one", "minimal", "guest", "reporter", "developer", "maintainer", "owner"
    EOF
  default     = {}
}

# Groups
variable "share_groups" {
  type = map(object({
    group_id     = number
    group_access = string
  }))
  description = <<-EOF
    Share the group with other groups.

    Example:
    {
      <name> = {
        group_id     = 12345
        group_access = "developer"
      }
    }
    Valid values: "no one", "minimal", "guest", "reporter", "developer", "maintainer", "owner"
    EOF
  default     = {}
}

# Service Accounts
variable "service_accounts" {
  type = map(object({
    name = optional(string)
  }))
  description = <<-EOF
    Group service accounts. The key is the username, the name defaults to username if not set.

    Example:
    {
      my-service-account = {
        name = "My Service Account"
      }
    }
    EOF
  default     = {}
}
