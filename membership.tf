# User
resource "gitlab_group_membership" "user" {
  for_each = var.members

  group_id = gitlab_group.group.id

  user_id      = each.key
  access_level = each.value.access_level
}

# Group
resource "gitlab_group_share_group" "group" {
  for_each = var.share_groups

  group_id = gitlab_group.group.id

  share_group_id = each.value.group_id
  group_access   = each.value.group_access
}
