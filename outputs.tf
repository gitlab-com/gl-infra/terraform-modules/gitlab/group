output "id" {
  description = "ID of the group."
  value       = gitlab_group.group.id
}

output "full_path" {
  description = "Full path of the group."
  value       = gitlab_group.group.full_path
}

output "member_ids" {
  description = "IDs of the members of the group."
  value       = keys(var.members)
}

output "service_account_ids" {
  description = "User IDs of the service accounts of the group."
  value = {
    for username in keys(var.service_accounts) :
    username => gitlab_group_service_account.service_account[username].service_account_id
  }
}
