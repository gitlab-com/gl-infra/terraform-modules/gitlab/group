# GitLab Group Terraform Module

## What is this?

This module provisions a GitLab group.

<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.4 |
| <a name="requirement_gitlab"></a> [gitlab](#requirement\_gitlab) | >= 17.7.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_gitlab"></a> [gitlab](#provider\_gitlab) | >= 17.7.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [gitlab_group.group](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/group) | resource |
| [gitlab_group_membership.user](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/group_membership) | resource |
| [gitlab_group_service_account.service_account](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/group_service_account) | resource |
| [gitlab_group_share_group.group](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/group_share_group) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_description"></a> [description](#input\_description) | The group’s description. | `string` | `null` | no |
| <a name="input_lfs_enabled"></a> [lfs\_enabled](#input\_lfs\_enabled) | Enable/disable Large File Storage (LFS) for the projects in this group. | `bool` | `true` | no |
| <a name="input_members"></a> [members](#input\_members) | Group memberships.<br/><br/>Example:<br/>{<br/>  <user\_id> = {<br/>    access\_level = "developer"<br/>  }<br/>}<br/>Valid values: "no one", "minimal", "guest", "reporter", "developer", "maintainer", "owner" | <pre>map(object({<br/>    access_level = string<br/>  }))</pre> | `{}` | no |
| <a name="input_name"></a> [name](#input\_name) | The name of the group. | `string` | `null` | no |
| <a name="input_parent_id"></a> [parent\_id](#input\_parent\_id) | The parent group ID for creating nested group. | `number` | `null` | no |
| <a name="input_path"></a> [path](#input\_path) | The path of the group. | `string` | n/a | yes |
| <a name="input_project_creation_level"></a> [project\_creation\_level](#input\_project\_creation\_level) | Determine if developers can create projects in the group. Can be `noone` (No one), `maintainer` (users with the Maintainer role), or `developer` (users with the Developer or Maintainer role). | `string` | `"maintainer"` | no |
| <a name="input_request_access_enabled"></a> [request\_access\_enabled](#input\_request\_access\_enabled) | Allow users to request member access. | `string` | `false` | no |
| <a name="input_require_two_factor_authentication"></a> [require\_two\_factor\_authentication](#input\_require\_two\_factor\_authentication) | Require all users in this group to setup Two-factor authentication. | `bool` | `false` | no |
| <a name="input_service_accounts"></a> [service\_accounts](#input\_service\_accounts) | Group service accounts. The key is the username, the name defaults to username if not set.<br/><br/>Example:<br/>{<br/>  my-service-account = {<br/>    name = "My Service Account"<br/>  }<br/>} | <pre>map(object({<br/>    name = optional(string)<br/>  }))</pre> | `{}` | no |
| <a name="input_share_groups"></a> [share\_groups](#input\_share\_groups) | Share the group with other groups.<br/><br/>Example:<br/>{<br/>  <name> = {<br/>    group\_id     = 12345<br/>    group\_access = "developer"<br/>  }<br/>}<br/>Valid values: "no one", "minimal", "guest", "reporter", "developer", "maintainer", "owner" | <pre>map(object({<br/>    group_id     = number<br/>    group_access = string<br/>  }))</pre> | `{}` | no |
| <a name="input_subgroup_creation_level"></a> [subgroup\_creation\_level](#input\_subgroup\_creation\_level) | Allowed to create subgroups. Can be `owner` (Owners), or `maintainer` (users with the Maintainer role). | `string` | `"maintainer"` | no |
| <a name="input_visibility_level"></a> [visibility\_level](#input\_visibility\_level) | The group’s visibility. Can be `private`, `internal`, or `public`. | `string` | `"public"` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_full_path"></a> [full\_path](#output\_full\_path) | Full path of the group. |
| <a name="output_id"></a> [id](#output\_id) | ID of the group. |
| <a name="output_member_ids"></a> [member\_ids](#output\_member\_ids) | IDs of the members of the group. |
| <a name="output_service_account_ids"></a> [service\_account\_ids](#output\_service\_account\_ids) | User IDs of the service accounts of the group. |
<!-- END_TF_DOCS -->

## Contributing

Please see [CONTRIBUTING.md](./CONTRIBUTING.md).

## License

See [LICENSE](./LICENSE).
