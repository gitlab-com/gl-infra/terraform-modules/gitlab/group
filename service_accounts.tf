resource "gitlab_group_service_account" "service_account" {
  for_each = var.service_accounts

  group    = gitlab_group.group.id
  name     = coalesce(each.value.name, each.key)
  username = each.key
}
