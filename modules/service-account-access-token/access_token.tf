resource "gitlab_group_service_account_access_token" "saat" {
  group   = var.group_id
  user_id = var.service_account_id

  name   = var.name
  scopes = var.scopes

  rotation_configuration = {
    expiration_days    = var.expires_after_days
    rotate_before_days = var.expires_after_days - var.rotate_after_days
  }
}

resource "gitlab_group_variable" "saat" {
  count = var.ci_variable != null ? 1 : 0

  group       = var.group_id
  key         = var.ci_variable.name
  description = join(" ", [var.ci_variable.description, "(IaC managed)"])
  value       = gitlab_group_service_account_access_token.saat.token
  protected   = var.ci_variable.protected
  masked      = true
  raw         = true # not expanded
}
