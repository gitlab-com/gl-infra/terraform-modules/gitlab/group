variable "group_id" {
  type        = string
  description = "ID of the group to create the Group Service Account Access Token in."
}

variable "service_account_id" {
  type        = number
  description = "User ID of the group service account to create the Service Account Access Token for."
}


variable "name" {
  type        = string
  description = "Name of the Group Service Account Access Token."
}

variable "scopes" {
  type        = set(string)
  description = "Scopes of the Group Service Account Access Token."

  validation {
    condition = setintersection([
      "api",
      "read_api",
      "read_registry",
      "write_registry",
      "read_repository",
      "write_repository",
      "create_runner",
      "manage_runner",
      "ai_features",
      "k8s_proxy",
      "read_observability",
      "write_observability"
    ], var.scopes) == var.scopes
    error_message = "The valid scope values are \"api\", \"read_api\", \"read_registry\", \"write_registry\", \"read_repository\", \"write_repository\", \"create_runner\", \"manage_runner\", \"ai_features\", \"k8s_proxy\", \"read_observability\", \"write_observability\"."
  }
}

variable "expires_after_days" {
  type        = number
  description = "Number of days before the Group Service Account Access Token expires."
  default     = 90
}

variable "rotate_after_days" {
  type        = number
  description = "Number of days before the Group Service Account Access Token is automatically rotated."
  default     = 50
}

variable "vault_kv_secret" {
  type = object({
    store        = optional(bool, false)
    mount        = optional(string, "ci")
    name         = optional(string, "")
    auth_path    = optional(string, "")
    group_path   = optional(string, "")
    path         = optional(string, "")
    username_key = optional(string, "username")
    token_key    = optional(string, "token")
    extra_data   = optional(map(any), {})
  })
  description = "Optionally store the Group Service Account Access Token into Vault. Either `path` or all of `auth_path`, `group_path`, and `name` must be set."
  default     = {}

  validation {
    condition = var.vault_kv_secret.store ? (
      var.vault_kv_secret.path != "" || !contains([var.vault_kv_secret.auth_path, var.vault_kv_secret.group_path, var.vault_kv_secret.name], "")
    ) : true
    error_message = "Either vault_kv_secret.path or all of vault_kv_secret.auth_path, vault_kv_secret.group_path and vault_kv_secret.name must be set when vault_kv_secret.store is enabled."
  }
}

variable "ci_variable" {
  type = object({
    name        = string
    description = optional(string)
    protected   = optional(bool, true)
  })
  description = "Optionally copy the Group Service Account Access Token into a CI variable."
  default     = null
  nullable    = true
}
