<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.4 |
| <a name="requirement_gitlab"></a> [gitlab](#requirement\_gitlab) | >= 17.7.0 |
| <a name="requirement_vault"></a> [vault](#requirement\_vault) | >= 3.9 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_gitlab"></a> [gitlab](#provider\_gitlab) | >= 17.7.0 |
| <a name="provider_vault"></a> [vault](#provider\_vault) | >= 3.9 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [gitlab_group_service_account_access_token.saat](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/group_service_account_access_token) | resource |
| [gitlab_group_variable.saat](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/group_variable) | resource |
| [vault_kv_secret_v2.saat](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/kv_secret_v2) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_ci_variable"></a> [ci\_variable](#input\_ci\_variable) | Optionally copy the Group Service Account Access Token into a CI variable. | <pre>object({<br/>    name        = string<br/>    description = optional(string)<br/>    protected   = optional(bool, true)<br/>  })</pre> | `null` | no |
| <a name="input_expires_after_days"></a> [expires\_after\_days](#input\_expires\_after\_days) | Number of days before the Group Service Account Access Token expires. | `number` | `90` | no |
| <a name="input_group_id"></a> [group\_id](#input\_group\_id) | ID of the group to create the Group Service Account Access Token in. | `string` | n/a | yes |
| <a name="input_name"></a> [name](#input\_name) | Name of the Group Service Account Access Token. | `string` | n/a | yes |
| <a name="input_rotate_after_days"></a> [rotate\_after\_days](#input\_rotate\_after\_days) | Number of days before the Group Service Account Access Token is automatically rotated. | `number` | `50` | no |
| <a name="input_scopes"></a> [scopes](#input\_scopes) | Scopes of the Group Service Account Access Token. | `set(string)` | n/a | yes |
| <a name="input_service_account_id"></a> [service\_account\_id](#input\_service\_account\_id) | User ID of the group service account to create the Service Account Access Token for. | `number` | n/a | yes |
| <a name="input_vault_kv_secret"></a> [vault\_kv\_secret](#input\_vault\_kv\_secret) | Optionally store the Group Service Account Access Token into Vault. Either `path` or all of `auth_path`, `group_path`, and `name` must be set. | <pre>object({<br/>    store        = optional(bool, false)<br/>    mount        = optional(string, "ci")<br/>    name         = optional(string, "")<br/>    auth_path    = optional(string, "")<br/>    group_path   = optional(string, "")<br/>    path         = optional(string, "")<br/>    username_key = optional(string, "username")<br/>    token_key    = optional(string, "token")<br/>    extra_data   = optional(map(any), {})<br/>  })</pre> | `{}` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_token"></a> [token](#output\_token) | The Group Service Account Access Token. |
| <a name="output_user_id"></a> [user\_id](#output\_user\_id) | User ID associated to the Group Service Account Access Token. |
| <a name="output_vault_secret_gitlab_path"></a> [vault\_secret\_gitlab\_path](#output\_vault\_secret\_gitlab\_path) | Vault path to the group service account access token in the short GitLab syntax, e.g. 'foo/bar/token@ci'. |
| <a name="output_vault_secret_name"></a> [vault\_secret\_name](#output\_vault\_secret\_name) | Name of the Vault secret, e.g. 'foo/bar'. |
| <a name="output_vault_secret_path"></a> [vault\_secret\_path](#output\_vault\_secret\_path) | Full path of the Vault secret including the mount, e.g. 'ci/foo/bar'. |
<!-- END_TF_DOCS -->