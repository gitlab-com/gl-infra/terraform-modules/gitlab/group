terraform {
  required_version = ">= 1.4"

  required_providers {
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = ">= 17.7.0"
    }
    vault = {
      source  = "hashicorp/vault"
      version = ">= 3.9"
    }
  }
}
