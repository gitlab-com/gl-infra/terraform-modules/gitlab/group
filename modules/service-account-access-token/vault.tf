resource "vault_kv_secret_v2" "saat" {
  count = var.vault_kv_secret.store ? 1 : 0

  mount = var.vault_kv_secret.mount
  name  = coalesce(var.vault_kv_secret.path, join("/", ["access_tokens", var.vault_kv_secret.auth_path, var.vault_kv_secret.group_path, "_service_account_access_tokens", var.vault_kv_secret.name]))

  data_json = jsonencode(merge(
    {
      (var.vault_kv_secret.username_key) = gitlab_group_service_account_access_token.saat.name
      (var.vault_kv_secret.token_key)    = gitlab_group_service_account_access_token.saat.token
    },
    var.vault_kv_secret.extra_data
  ))

  delete_all_versions = true
}
