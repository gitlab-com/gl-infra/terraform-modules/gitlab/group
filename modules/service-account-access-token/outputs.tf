output "user_id" {
  description = "User ID associated to the Group Service Account Access Token."
  value       = gitlab_group_service_account_access_token.saat.user_id
}

output "token" {
  description = "The Group Service Account Access Token."
  value       = gitlab_group_service_account_access_token.saat.token
  sensitive   = true
}

output "vault_secret_path" {
  description = "Full path of the Vault secret including the mount, e.g. 'ci/foo/bar'."
  value       = var.vault_kv_secret.store ? join("/", [vault_kv_secret_v2.saat[0].mount, vault_kv_secret_v2.saat[0].name]) : null
}

output "vault_secret_name" {
  description = "Name of the Vault secret, e.g. 'foo/bar'."
  value       = var.vault_kv_secret.store ? vault_kv_secret_v2.saat[0].name : null
}

output "vault_secret_gitlab_path" {
  description = "Vault path to the group service account access token in the short GitLab syntax, e.g. 'foo/bar/token@ci'."
  value       = var.vault_kv_secret.store ? "${vault_kv_secret_v2.saat[0].name}/${var.vault_kv_secret.token_key}@${vault_kv_secret_v2.saat[0].mount}" : null
}
