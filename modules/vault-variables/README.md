<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.4 |
| <a name="requirement_gitlab"></a> [gitlab](#requirement\_gitlab) | >= 17.7.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_gitlab"></a> [gitlab](#provider\_gitlab) | >= 17.7.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [gitlab_group_variable.vault_addr](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/group_variable) | resource |
| [gitlab_group_variable.vault_auth_path](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/group_variable) | resource |
| [gitlab_group_variable.vault_secrets_shared_path](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/group_variable) | resource |
| [gitlab_group_variable.vault_server_url](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/group_variable) | resource |
| [gitlab_group_variable.vault_transit_path](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/group_variable) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_group_id"></a> [group\_id](#input\_group\_id) | ID of the group to create the variables in. | `number` | n/a | yes |
| <a name="input_vault"></a> [vault](#input\_vault) | n/a | <pre>object({<br/>    addr            = optional(string, "https://vault.ops.gke.gitlab.net")<br/>    auth_path       = string<br/>    ci_shared_path  = optional(string, "shared")<br/>    ci_transit_path = optional(string, "transit/ci")<br/>  })</pre> | n/a | yes |

## Outputs

No outputs.
<!-- END_TF_DOCS -->