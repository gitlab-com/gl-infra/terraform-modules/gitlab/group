resource "gitlab_group_variable" "vault_addr" {
  group             = var.group_id
  key               = "VAULT_ADDR"
  value             = var.vault.addr
  description       = "(IaC managed)"
  protected         = false
  masked            = false
  raw               = true
  environment_scope = "*"
}

resource "gitlab_group_variable" "vault_server_url" {
  group             = var.group_id
  key               = "VAULT_SERVER_URL"
  value             = var.vault.addr
  description       = "(IaC managed)"
  protected         = false
  masked            = false
  raw               = true
  environment_scope = "*"
}

resource "gitlab_group_variable" "vault_auth_path" {
  group             = var.group_id
  key               = "VAULT_AUTH_PATH"
  value             = var.vault.auth_path
  description       = "(IaC managed)"
  protected         = false
  masked            = false
  raw               = true
  environment_scope = "*"
}

resource "gitlab_group_variable" "vault_secrets_shared_path" {
  group             = var.group_id
  key               = "VAULT_SECRETS_SHARED_PATH"
  value             = var.vault.ci_shared_path
  description       = "Path to shared secrets. (IaC managed)"
  protected         = false
  masked            = false
  raw               = true
  environment_scope = "*"
}

resource "gitlab_group_variable" "vault_transit_path" {
  group             = var.group_id
  key               = "VAULT_TRANSIT_PATH"
  value             = var.vault.ci_transit_path
  description       = "Path to the transit secret engine. (IaC managed)"
  protected         = false
  masked            = false
  raw               = true
  environment_scope = "*"
}
