output "user_id" {
  description = "User ID associated to the Group Access Token."
  value       = gitlab_group_access_token.gat.user_id
}

output "token" {
  description = "The Group Access Token."
  value       = gitlab_group_access_token.gat.token
  sensitive   = true
}

output "vault_secret_path" {
  description = "Full path of the Vault secret including the mount, e.g. 'ci/foo/bar'."
  value       = var.vault_kv_secret.store ? join("/", [vault_kv_secret_v2.gat[0].mount, vault_kv_secret_v2.gat[0].name]) : null
}

output "vault_secret_name" {
  description = "Name of the Vault secret, e.g. 'foo/bar'."
  value       = var.vault_kv_secret.store ? vault_kv_secret_v2.gat[0].name : null
}

output "vault_secret_gitlab_path" {
  description = "Vault path to the group access token in the short GitLab syntax, e.g. 'foo/bar/token@ci'."
  value       = var.vault_kv_secret.store ? "${vault_kv_secret_v2.gat[0].name}/${var.vault_kv_secret.token_key}@${vault_kv_secret_v2.gat[0].mount}" : null
}
