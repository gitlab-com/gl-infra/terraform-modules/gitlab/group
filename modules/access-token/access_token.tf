resource "gitlab_group_access_token" "gat" {
  group = var.group_id

  name         = var.name
  access_level = var.access_level
  scopes       = var.scopes

  rotation_configuration = {
    expiration_days    = var.expires_after_days
    rotate_before_days = var.expires_after_days - var.rotate_after_days
  }
}

resource "gitlab_group_variable" "gat" {
  count = var.ci_variable != null ? 1 : 0

  group       = var.group_id
  key         = var.ci_variable.name
  description = join(" ", [var.ci_variable.description, "(IaC managed)"])
  value       = gitlab_group_access_token.gat.token
  protected   = var.ci_variable.protected
  masked      = true
  raw         = true # not expanded
}
