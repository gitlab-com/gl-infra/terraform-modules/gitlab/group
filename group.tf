resource "gitlab_group" "group" {
  name        = coalesce(var.name, var.path)
  path        = var.path
  description = var.description

  parent_id = var.parent_id

  visibility_level        = var.visibility_level
  project_creation_level  = var.project_creation_level
  request_access_enabled  = var.request_access_enabled
  subgroup_creation_level = var.subgroup_creation_level

  lfs_enabled = var.lfs_enabled

  require_two_factor_authentication = var.require_two_factor_authentication

  lifecycle {
    # Can only be set by administrators
    ignore_changes = [shared_runners_minutes_limit]
  }
}
